import json
import pandas


data = json.load(open('ne-data.txt'))

# читаем поддокумент, описывающий интерфейсы
interfaces = data['task_result']['content']['network-element'][0]['interface']

resdict = {}
# извлекаем словари только по портам с поднятым линком
for n, interface in enumerate(interfaces):
    if interface['oper-status'] == 'up':  # присутствие линка на порту
        location = interface['name'].split('/')  # [описание+стойка, полка, слот, порт]
        try:  # интерфейсы Loopback роняют код по исключению IndexError в условии, за одно и исключаем их из выборки
            if str(location[1]).isdigit():  # если это не порт управления
                resdict[n] = (location[-4][-1],   # номер стойки
                              location[-3],       # номер полки
                              location[-2],       # номер слота
                              location[-1],       # номер порта
                              interface['oper-status'],  # состояние линка
                              interface['name'])         # имя интерфейса
        except IndexError:
            pass

# передаём словарь с результатом в pandas датафрейм для сортировки
result = pandas.DataFrame.from_dict(resdict, columns=['Cabinet',
                                                      'Subrack',
                                                      'Slot',
                                                      'Port',
                                                      'Status',
                                                      'Name'], orient='index')
# сортировка
result = result.sort_values(by='Port')
result = result.sort_values(by='Slot')
result = result.sort_values(by='Subrack')
result = result.sort_values(by='Cabinet')
result = result.reset_index()  # проставляем новые индексы
result = result.drop(columns=['index'])  # избавляемся от старых индексов, наследованных из interfaces

output = open('output.txt', 'w')
prev = {}  # расположение предыдущего интерфейса в цикле
for i, row in result.iterrows():
    if i != 0:
        if row.Cabinet == prev.Cabinet:      # если та же стойка
            if row.Subrack == prev.Subrack:  # если та же полка
                if row.Slot == prev.Slot:    # если тот же слот
                    output.write(f'{15 * " "}Port: {row.Port}, Status: {row.Status}, Name: {row.Name}\n')
                else:
                    output.write(f'{11 * " "}Slot: {row.Slot}\n'
                                 f'{15 * " "}Port: {row.Port}, Status: {row.Status}, Name: {row.Name}\n')
            else:
                output.write(f'{4 * " "}Subrack: {row.Subrack}\n'
                             f'{11 * " "}Slot: {row.Slot}\n'
                             f'{15 * " "}Port: {row.Port}, Status: {row.Status}, Name: {row.Name}\n')
        else:
            output.write(f'Cabinet: {row.Cabinet}\n'
                         f'{4 * " "}Subrack: {row.Subrack}\n'
                         f'{11 * " "}Slot: {row.Slot}\n'
                         f'{15 * " "}Port: {row.Port}, Status: {row.Status}, Name: {row.Name}\n')
    else:
        output.write(f'Cabinet: {row.Cabinet}\n'
                     f'{4 * " "}Subrack: {row.Subrack}\n'
                     f'{11 * " "}Slot: {row.Slot}\n'
                     f'{15 * " "}Port: {row.Port}, Status: {row.Status}, Name: {row.Name}\n')

    prev = row

output.close()
print('\n\n Result has been saved to output.txt\n')
input(" Press Enter to exit...")
